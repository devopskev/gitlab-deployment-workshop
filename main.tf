provider "aws" {
  region = "us-east-1"
}

provider "tls" {}
provider "random" {}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

terraform {
  required_version = "0.12.29"
}

locals {
  gitlab_version                 = "gitlab-ee-13.7.1"
  region                         = "us-east-1"
  vpc_azs                        = ["us-east-1a", "us-east-1b", "us-east-1c"]
  public_subnets                 = module.vpc.public_subnets
  private_subnets                 = module.vpc.private_subnets
  prefix                         = "aws-tf"
  vpc_name                       = module.vpc.name
  vpc_id                         = module.vpc.vpc_id
  vpc_cidr                       = module.vpc.vpc_cidr_block
  ami_id                       = data.aws_ami.latest-redhat.id
  app_instance_type              = "t3.medium"
  gitaly_instance_type           = "t3.large"
  praefect_instance_type         = "t3.large"
  praefect_port                  = 2305
  gitaly_port                    = 8075
  public_key = aws_key_pair.deployer.key_name
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDE1PtAMo9L1+H2WG5qVtcWZgZKcMjYpHKXzTKcq6NhGOVDdZX/0V9/9yqHcwuVt0qej6s8Mf112FpOI0ZKEY8y3EP6ZWVBQ63Auap7m63SxDfqtsRNg1FgeXAf1ksFp4SNyLj96PABaOb5oXA/Ez9qhYyeG/G14/5MM2iMa+np+EpBpPgNna5cIxMjnX1AxWBausbd2laGM5Sp/jdllDRgXouckSQDKXVfSQ4m8CA5sLmzP5j2dxXxdwZSEMXbk4JspI0R31+URm758FjsH4mFyZV2CS629eMxdnt9A/orG/QnbRGKQ8s9IlHMPXmdsnp/Hdjc+qZyRmJIAt6LlSqnqhAs8BBpbTFn46PA67jIhTVg+nad4lMiCLR55+H1V6XV4m7nwk47T67MH+0OWPb2wXXKvSoQa3JQ1zuG+kalla+uHwpno3G1y5iusYR+v+qDlWrsGHBfOFaAbHC9trx79/jnbJ46bbG9uKXqSJm6VFZFK7BqrvRIDD8HTQLYD8Y3ZpkbvD93MovAvpLaSdDCF1KWPUF/EXNTu6Ju0TEIQP0YrWWROifqfnseuZqF2/v1GOoA66k8YhATFryA530pgIDnui5Wg1j16b0xKHXxBBpknKBvRhoB0Sr75D3DXdPntZuFiKsONKzHZrHxeVein7tqN1RoYywmSbTdUTd4Xw== kvogt@gitlab.com"
}

module "s3" {
  source            = "./modules/s3"
  prefix            = local.prefix
  gitlab_s3_buckets = ["artifacts", "externaldiffs", "lfs", 
                    "uploads", "packages", "dependency-proxy",
                    "tfstate", "backup"] 
}


module "rds" {
  source                    = "./modules/rds"
  storage_type              = "gp2"
  allocated_storage         = 20
  engine                    = "postgres"
  engine_version            = "11.9"
  instance_class            = "db.t3.medium"
  final_snapshot_identifier = "gitlab"
  prefix                    = local.prefix
  vpc_id                    = local.vpc_id
  vpc_cidr                  = local.vpc_cidr
  multi_az                   = false # should be true for production
  auto_minor_version_upgrade = true
  private_subnets            = module.vpc.private_subnets #[
}

module "redis" {
  source                = "./modules/redis"
  node_type             = "cache.t3.medium"
  number_cache_clusters = 2
  prefix                = local.prefix
  vpc_id                = local.vpc_id
  vpc_cidr              = local.vpc_cidr
  private_subnets = module.vpc.private_subnets #[
}



module "rails" {
  source = "./modules/rails_noasg_noconfig"
  rails_count = 2
  ami_id = local.ami_id
  key_name = local.public_key
  instance_type = "t3.large"
  private_subnets = local.private_subnets
  public_subnets = local.public_subnets
  vpc_id = local.vpc_id 
  vpc_cidr = local.vpc_cidr
  prefix            = local.prefix
}

module "gitaly_cluster" {
  source = "./modules/gitaly_cluster_noconfig"
  ami_id = local.ami_id
  key_name = local.public_key
  gitaly_instance_type = "t3.large"
  praefect_instance_type = "t3.large"
  private_subnets = local.private_subnets
  vpc_id = local.vpc_id 
  vpc_cidr = local.vpc_cidr
  praefect_ips = ["10.0.1.100", "10.0.2.100", "10.0.3.100"]
  git_size = 100
  praefect_dbadmin = "gitlabadmin"
  praefect_port = 2305
}

module "bastion" {
  source = "./modules/bastion"
  ami_id = local.ami_id
  key_name = local.public_key
  subnet_id = local.public_subnets[0]
  vpc_id = local.vpc_id

}

########################################################################
#      Gitlab Testing
########################################################################
module "vpc" {
  source             = "terraform-aws-modules/vpc/aws"
  name               = "my-vpc"
  cidr               = "10.0.0.0/16"
  azs                = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets    = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets     = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
  enable_nat_gateway = true
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

data "aws_ami" "latest-ubuntu" {
most_recent = true
owners = ["099720109477"] # Canonical

  filter {
      name   = "name"
      values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
      name   = "virtualization-type"
      values = ["hvm"]
  }
}

# output latest_ami_ubuntu {
#   value = data.aws_ami.latest-ubuntu.id
# }

data "aws_ami" "latest-redhat" {
most_recent = true
owners = ["309956199498"] 

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  filter {
      name   = "virtualization-type"
      values = ["hvm"]
  }
  filter {
    name   = "name"
    values = ["RHEL-8*"]
  }
}

# output latest_ami_rhel {
#   value = data.aws_ami.latest-redhat.id
# }

data "aws_ami" "latest-centos" {
owners      = ["679593333241"]
most_recent = true

  filter {
      name   = "name"
      values = ["CentOS Linux 7 x86_64 HVM EBS *"]
  }

  filter {
      name   = "architecture"
      values = ["x86_64"]
  }

  filter {
      name   = "root-device-type"
      values = ["ebs"]
  }
}

# output latest_ami_centos {
#   value = data.aws_ami.latest-centos.id
# }

########################################################################

