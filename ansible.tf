# terraform output ansible > ../../OnPrem/Ansible/Redhat/inventory/terraform

locals {
  ansible = <<EOF
---
# terraform output ansible > ../../OnPrem/Ansible/Redhat/inventory/terraform.yml
all:
  vars:
    ansible_user: 'ec2-user'
    ansible_ssh_pass: '~/.ssh/id_rsa'
    ansible_ssh_common_args: '-o StrictHostKeyChecking=no'

    external_url: "http://${module.rails.lb_url}"
    redirect_http_to_https: false
    redis_master: "${module.redis.redis_endpoint}"
    initial_rails: ${module.rails.initial_rails_ip}
    gitaly_host: "${module.gitaly_cluster.praefect_endpoint}"
    gitaly_host_1: ${module.gitaly_cluster.gitaly_private_ip[0]}
    gitaly_host_2: ${module.gitaly_cluster.gitaly_private_ip[1]}
    gitaly_host_3: ${module.gitaly_cluster.gitaly_private_ip[2]}

    praefect_host: "${module.gitaly_cluster.praefect_endpoint}"
    praefect_int_token: ADSFJKLDSJFKLAJEIDSAFJFDASKLJ   
    praefect_ext_token: adfsfasfsasfafsdafadrgwhwe
    praefect_tls: false
    gitaly_token: asfefffwewjiojisc
    praefect_protocol: tcp
    praefect_port: 2305



    monitoring_whitelist: "0.0.0.0/0"
    init_db : true

    app_repo_path: "/etc/yum.repos.d/gitlab_gitlab-ee.repo"
    app_repo_url: https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh
    gitlab_pkg: gitlab-ee-13.7.2

  children:
    gitlab_app:
      hosts:
%{for host in module.rails.rails_ips}
        ${host}: ~
%{endfor}
      vars:
        postgres_user: "${module.rds.rds_admin_user}"
        pgbouncer_users_pg_bouncer_pw: "${module.rds.rds_admin_pass}"
        pg_password: "${module.rds.rds_admin_pass}"
        pgbouncer_host: "${module.rds.rds_endpoint}"
        db_port: 5432
        redis_master: "${module.redis.redis_endpoint}"
        
        object_store_provider: "AWS"
        object_storage_region: "${local.region}"
        artifacts_bucket: "${module.s3.artifacts}"
        externaldiffs_bucket: "${module.s3.external_diffs}"
        lfs_bucket: "${module.s3.lfs}"
        uploads_bucket: "${module.s3.uploads}"
        packages_bucket: "${module.s3.packages}"
        dependencyproxy_bucket: "${module.s3.dependency-proxy}"
        terraformstate_bucket: "${module.s3.tfstate}"
        backup_bucket: "${module.s3.backup}"
        redis_port: 6379
        redis_ssl: false

    praefect:
      hosts:
%{for host in module.gitaly_cluster.praefect_private_ip}
        ${host}: ~
%{endfor}
      vars:
        praefect_db_host: "${module.gitaly_cluster.rds_endpoint}"
        postgres_user: "${module.gitaly_cluster.rds_admin_user}"
        praefect_sql_password: "${module.gitaly_cluster.rds_admin_pass}"
        gitaly_protocol: tcp
        gitaly_port: 8075

    gitaly:
      hosts:
%{for host in module.gitaly_cluster.gitaly_private_ip}
        ${host}: ~
%{endfor}
  EOF
}


output "ansible" {
  value = "${local.ansible}"
}
