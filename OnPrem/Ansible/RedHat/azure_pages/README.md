Role Name
=========

This Role enables rails to use Azure Files Shares to mount GitLab Pages.

Requirements
------------

An Azure File Share Provisioned

Role Variables
--------------

azure_sp                            : Azure Service Account Principal (User Name for SP)
azure_sp_pwd                        : The Service Principal Password
azure_tenant                        : The Azure Tenant for Login
object_storage_access_key_id        : Storage account access key (User Name)
object_storage_secret_access_key    : The storage account secret key form the Azure Portal


Dependencies
------------

Azure Storage Account with File Shares Enabled
Ability to install azure-cli and cifs-utils form YUM

Author Information
------------------

kvogt@gitlab.com