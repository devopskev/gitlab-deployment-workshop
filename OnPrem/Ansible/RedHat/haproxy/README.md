Role Name
=========

This role Configures HAProxy. Initial implementation was very specific and retired.

Requirements
------------

Configure both internal and external templates per the requirements.
A Group called praefect in inventory
A Group called gitlab_app in inventory

Role Variables
--------------

gitlab_dns:                     DNS of Gitlab (Ex gitlab.com)


Dependencies
------------

None 

Author Information
------------------

kvogt@gitlab.com