Role Name
=========

Installs GitLab Repo and GitLab

Requirements
------------


Role Variables
--------------

    app_repo_path: "/etc/yum.repos.d/gitlab_gitlab-ee.repo"
    app_repo_url: https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh
    gitlab_pkg: gitlab-ee-13.7.2

Dependencies
------------


Example
------------


Author Information
------------------

kvogt@gitlab.com


