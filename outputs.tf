output "Bastion_IP"{
    value = module.bastion.public_ip
}

output "rails_ips" {
    value = module.rails.rails_ips
}

output "initial_rails_ip" {
    value = module.rails.initial_rails_ip
}

output "lb_url" {
    value = module.rails.lb_url
}

output "redis_endpoint" {
  value = module.redis.redis_endpoint
}
output "rds_endpoint"{
    value = module.rds.rds_endpoint
}

output "rds_db_name"{
    value = module.rds.rds_db_name
}

output "rds_admin_user"{
    value = module.rds.rds_admin_user
}

output "rds_admin_pass"{
    value = module.rds.rds_admin_pass
}

output "artifacts" {
  value = module.s3.artifacts
}

output "external_diffs" {
  value = module.s3.external_diffs
}

output "lfs" {
  value = module.s3.lfs
}

output "uploads" {
  value = module.s3.uploads
}

output "packages" {
  value = module.s3.packages
}

output "dependency-proxy" {
  value = module.s3.dependency-proxy
}

output "tfstate" {
  value = module.s3.tfstate
}

output "backup" {
  value = module.s3.backup
}

output "gitaly_private_ip"{
  value = module.gitaly_cluster.gitaly_private_ip
}

output "praefect_private_ip"{
  value = module.gitaly_cluster.praefect_private_ip
}

output "praefect_endpoint"{
  value = module.gitaly_cluster.praefect_endpoint
}

output "praefect_db_endpoint" {
  value = module.gitaly_cluster.rds_endpoint
}

output "praefect_rds_db_name" {
  value = module.gitaly_cluster.rds_db_name
}

output "praefect_rds_admin_user" {
  value = module.gitaly_cluster.rds_admin_user
}

output "praefect_rds_admin_pass" {
  value = module.gitaly_cluster.rds_admin_pass
}
