resource "random_string" "random" {
  length  = 3
  special = false
  number  = false
}

resource "aws_security_group" "praefect-pg-security-group" {
  name   = "praefect-pg-sg"
  vpc_id = var.vpc_id
  ingress {
    description = "Access from VPC"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "random_string" "password" {
  length  = 32
  special = false
}

resource "aws_secretsmanager_secret" "gitlab-praefectpg-password" {
  name = "gitlab-praefectpg-admin-pass-${random_string.random.result}"
}

resource "aws_secretsmanager_secret_version" "gitlab-postgres-password" {
  secret_id     = aws_secretsmanager_secret.gitlab-praefectpg-password.id
  secret_string = random_string.password.result
}

resource "aws_db_subnet_group" "private-subnets" {
  name       = "praefect-db-subnet-group"
  subnet_ids = var.private_subnets
}

resource "aws_db_instance" "gitlab-pg" {
  allocated_storage          = 20
  storage_type               = "gp2"
  engine                     = "postgres"
  engine_version             = "12"
  instance_class             = "db.t3.medium"
  name                       = "praefect_production"
  username                   = var.praefect_dbadmin
  password                   = aws_secretsmanager_secret_version.gitlab-postgres-password.secret_string
  multi_az                   = var.praefect_multiaz
  auto_minor_version_upgrade = true
  final_snapshot_identifier  = "praefect-${random_string.random.result}"
  db_subnet_group_name       = aws_db_subnet_group.private-subnets.name
  vpc_security_group_ids     = [aws_security_group.praefect-pg-security-group.id]
}
