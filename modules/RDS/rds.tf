resource "random_string" "random" {
  length  = 3
  special = false
  number  = false
}

resource "aws_security_group" "postgres-security-group" {
  name   = join("-", [var.prefix, "postgres-sg"])
  vpc_id = var.vpc_id
  ingress {
    description = "Access from VPC"
    from_port   = var.postgres_port
    to_port     = var.postgres_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "random_string" "password" {
  length  = 32
  special = false
}
resource "aws_secretsmanager_secret" "gitlab-postgres-password" {
  name = join("-", [var.prefix, "gitlab-postgres-admin-pass", "${random_string.random.result}"])
}
resource "aws_secretsmanager_secret_version" "gitlab-postgres-password" {
  secret_id     = aws_secretsmanager_secret.gitlab-postgres-password.id
  secret_string = random_string.password.result
}


resource "aws_db_subnet_group" "private-subnets" {
  name       = join("-", [var.prefix, "gitlab-db-subnet-group"])
  subnet_ids = var.private_subnets
}

resource "aws_db_instance" "gitlab-pg" {
  allocated_storage          = var.allocated_storage
  storage_type               = var.storage_type
  engine                     = var.engine
  engine_version             = var.engine_version
  instance_class             = var.instance_class
  name                       = var.database_name
  username                   = var.database_admin
  password                   = aws_secretsmanager_secret_version.gitlab-postgres-password.secret_string
  multi_az                   = var.multi_az
  auto_minor_version_upgrade = var.auto_minor_version_upgrade
  final_snapshot_identifier  = var.final_snapshot_identifier
  db_subnet_group_name       = aws_db_subnet_group.private-subnets.name
  vpc_security_group_ids     = [aws_security_group.postgres-security-group.id]
}