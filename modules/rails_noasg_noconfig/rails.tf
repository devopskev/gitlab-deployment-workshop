resource "aws_security_group" "rails" {
  name        = "gitlab-app-sg"
  description = "Rails Security Group"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_instance" "rails" {
  count         = var.rails_count
  instance_type = var.instance_type
  ami           = var.ami_id
  key_name      = var.key_name
  subnet_id     = element(var.private_subnets, count.index)
  ebs_optimized = true

  iam_instance_profile   = aws_iam_instance_profile.rails.name
  vpc_security_group_ids = [aws_security_group.rails.id]
  tags = {
    Name = "Rails-${count.index + 1}"
  }
  user_data = base64encode(templatefile("${path.module}/templates/user_data.yml",{}))
}


resource "aws_iam_instance_profile" "rails" {
  name = "gitlab-app-nodes"
  path = aws_iam_role.gitlab-app.path
  role = aws_iam_role.gitlab-app.name
}

resource "aws_iam_role" "gitlab-app" {
  name               = "gitlab-app-nodes"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.gitlab-app-assume-role.json 
}

resource "aws_iam_role_policy" "gitlab-app" {
  policy = data.aws_iam_policy_document.gitlab-app.json
  role   = aws_iam_role.gitlab-app.name
  name   = "gitlab-app-nodes"
}

data "aws_iam_policy_document" "gitlab-app-assume-role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      identifiers = ["ec2.amazonaws.com"]
      type        = "Service"
    }
  }
}

data "aws_iam_policy_document" "gitlab-app" {
  statement {
    effect = "Allow"

    actions = ["s3:GetObject",
      "s3:PutObject",
    "s3:DeleteObject"]

    resources = ["arn:aws:s3:::${var.prefix}-*/*"]
  }
  statement {
    effect = "Allow"

    actions = ["s3:ListAllMyBuckets"]

    resources = ["*"]
  }
}


# This is just for GitLab Internal testing so a key pair is not needed
resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.gitlab-app.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}