variable "prefix" {}

variable "private_subnets" {}

variable "vpc_id" {
  type = string
}

variable "cluster_name" {
  type    = string
  default = "gitlab-redis"
}

variable "vpc_cidr" {
  type = string
}

variable "tags" {
  type        = map(string)
  description = "map of tags to put on the resource"
  default     = {}
}

variable "number_cache_clusters" {
  type = string
}

variable "node_type" {
  type = string
}