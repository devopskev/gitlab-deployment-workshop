:exclamation: Name the title of this issue: `<Your Name> - <Your company>` Deployment Workshop

# Deploying GitLab HA with Terraform and ansible
_This tutorial uses AWS cloud managed services for some of the GitLab components rather than the omnibus installer._

> Make sure to checkout the [Local Environment Setup and Cloud Access](#local-environment-setup-and-cloud-access) to setup pre-requisites `tfenv`, `ansible`, and an AWS account. 

1. [ ] Create a SSH key pair or reuse `~/.ssh/id_rsa`
2. [ ] Update ansible.tf ansible_ssh_pass to point to your private (not .pub extension)
3. [ ] Update main.tf (line 37) with the contents for your public key (Ex: ~/.ssh/id_rsa.pub)
4. [ ] Run `terraform init`
5. [ ] Run `terraform apply`
> Answer yes, you can 'echo yes|terraform apply' to automate
6. [ ] run `terraform apply` again
> This is a known bug in terraform https://github.com/terraform-providers/terraform-provider-azurerm/issues/764. To summarize the ip isn't attached yet when the output happens. Should have happened by the second run.
7. [ ] `terraform output ansible > ../OnPrem/Ansible/Redhat/inventory/terraform.yml`

8. [ ] Run `terraform output lb_url`. This will show you the url of the loadbalancer needed for later steps. Copy it to a scratch pad.  
9. [ ] Run `terraform output Bastion_IP`. This will show the Bastion Host IP that is needed for later steps.  Copy it to a scratch pad.
10. [ ] Run `terraform output rails_ips`. This is the IP addresses for the Rails instances that are needed in a later step. Copy to a scratch pad. 
> This is your GitLab url (Save for Later), your Bastion IP, and your Rails IPs
11. [ ] Add this to your ~/.ssh/config. Replace the IP in hostname with your bastion IP from output above and the path to your private key pair (IdentityFile)
```
Host awsbastion
  Hostname 52.5.25.197
  ForwardAgent yes
  IdentityFile ~/.ssh/id_rsa
  ControlPath ~/.ssh/control/%C
  ControlMaster auto
  ControlPersist 10m

Host 10.0.*
  IdentityFile ~/.ssh/id_rsa
  ProxyCommand ssh ec2-user@awsbastion -W %h:%p
  ControlPersist 10m
```

12. [ ] SSH into the Rails Instances to add them to the known_hosts list
13. [ ] Move to the RedHat directory `cd OnPrem/Ansible/Redhat`
14. [ ] Run `ansible-playbook -i inventory gl_rails.yml` 
15. [ ] Run `ansible-playbook -i inventory gl_gitaly.yml gl_praefect.yml`
16. [ ] Navigate to the AWS console. Take a screenshot of the GitLab instances running. Post the screen shot as a comment in this issue.
17. [ ] Log into GitLab using the url saved from above. Validate the instance by updating the root password, logging in and performing some standard workflows. 
    - [ ] :white_check_mark: make sure to save this url to add to the certification form in a later step. Leave this instance running so the GitLab team can verify it.
    - [ ] Post screenshots of the admin screen of the newly created GitLab instance as comments in this issue. 

## Clean-up
_Note, if you're taking this as part of the implementation engineer, please leave your environment up for 10 business days to allow for grading_. 

18. [ ] Go back to the terraform directory and 'terraform destroy'
> If you uploaded attachments you will need to manually clear out all S3 buckets and version history before Terraform will destroy the resources. 'terraform show' can be run to see what resources are outstanding. You may also receive an error on outputs. This is ok. It's just not able to output resources that no longer exist.
19. [ ] Log into AWS Console > RDS > Snapshots. Delete the final snapshots taken.

## Troubleshooting 
1. If you get the error `Failed to create temporary directory on SSH Connection`, You may need to create a control directory on MacOS with 700 permissions. https://discussions.apple.com/thread/7826165

1. You man also need to install `brew install hudochenkov/sshpass/sshpass`. https://stackoverflow.com/questions/32255660/how-to-install-sshpass-on-mac 

# Local Environment Setup and Cloud Access

## Access - For Gitlab team members only
_If you're a partner on the Certified Implementation Engineer journey, we expect you to use your company's AWS cloud account for this workshop._ 
1. Check AWS access by:
    - In a browser, navigate to https://gitlabsandbox.cloud/login and click `Login using Okta`
    - Navigate to the `Cloud Infrastcutre` tab and follow the steps to create an account for AWS. 
    - Once your account is provisioned (refresh the page to see status updates), click `Open AWS Web Console` button in the top right. 
    - When prompted for `IAM user name` and Password, you can grab from the page you just came from after clicking the `View IAM Credentials` button (top right).

## Install packages and dependencies
1. Install  [tfenv](https://github.com/tfutils/tfenv) by using `brew install tfenv`. 
1. Install [ansible](https://hvops.com/articles/ansible-mac-osx/) using `brew install ansible`.
1. [Install AWS CLIv2](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-mac.html#cliv2-mac-install-gui)
1. Configure your AWS cli by running `aws configure`. 
    - The Access Key and Secret Key can be found from the [AWS Console](https://console.aws.amazon.com/iam/home?region=us-east-1#/security_credentials). 
    - The default region should be `us-east-1`.
    - Leave the default output option blank. 
